alias pins='sudo pacman -S'
alias pup='sudo pacman -Syu'
alias yins='yay -S'
alias vim='nvim'
alias fullclean='make clean && rm -f config.h && git reset --hard origin/master'
alias config='/usr/bin/git --git-dir=$HOME/ArchDots/ --work-tree=$HOME'

pfetch

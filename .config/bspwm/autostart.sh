function run {
	if ! pgrep $1 ;
	then
		$@1 ;
	fi
}

# Launch composite manager
  pgrep -u "$USER" compton >/dev/null || \
      compton -bcCG -o 0.2 -r 0 -l 5 -t 5

# Start Polybar
$HOME/.config/polybar/launch.sh &

# Set terminal colour scheme
  wal -R &

# Set wallpaper
  nitrgen --restore &

# Start hotkey daemon
  sxhkd &

# Start Polkit
  /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# Mount OneDrive Partition
  sh -c "rclone --vfs-cache-mode writes mount onedrive: ~/OneDrive"

# Start nm-applet
  nm-applet &

### Font installation instructions

* Copy fold into /usr/share/fonts
* Change file permissions if required:
	`chmod 644 /usr/share/fonts/WindowsFonts`
* Regenerate fontconfig cache:
	`fc-cache -f`
